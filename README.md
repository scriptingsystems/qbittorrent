# Introduction

Fuente de imagen de contenedor: https://github.com/linuxserver/docker-qbittorrent
Fuente de repositorio de enlaces torrent: https://btdig.com


# Environment

1. Basandonos en la imagen de **lscr.io/linuxserver/qbittorrent** podemos podemos levantar el servicio qbittorrent para que sea nuestro cliente torrent
```bash
podman run -d \
  --name=qbittorrent \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ=Etc/UTC \
  -e WEBUI_PORT=8080 \
  -e TORRENTING_PORT=6881 \
  -p 8080:8080 \
  -p 6881:6881 \
  -p 6881:6881/udp \
  -v qbittorrent_config:/config \
  -v qbittorrent_downloads:/downloads \
  --restart unless-stopped \
  lscr.io/linuxserver/qbittorrent:latest
```

2. Desde la web https://btdig.com buscamos el torrent de nuestro interes. Y copiamos el enlace del apartado de Download

3. En qbittorrent en **File** >> **Add torrent link...**, pegamos nuestro enlace
